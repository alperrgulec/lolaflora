package com.ciceksepeti.demo.base;

import com.ciceksepeti.demo.DemoApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterClass;

@SpringBootTest(classes = DemoApplication.class)
public class AbstractTest extends AbstractTestNGSpringContextTests {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @AfterClass
    public void close() {
        DriverFactory.getInstance().driver.remove();
    }

}
