package com.ciceksepeti.demo.base;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.HashMap;

public enum DriverType implements DriverSetup {

    CHROME{
        @Override
        public RemoteWebDriver getWebDriverObject(DesiredCapabilities desiredCapabilities) {
            System.setProperty("webdriver.chrome.driver", "src/test/resources/driver/geckodriver");
            HashMap<String, Object> chromePreferences = new HashMap<>();
            chromePreferences.put("profile.password_manager_enabled", false);

            FirefoxOptions options = new FirefoxOptions();
//            options.merge(desiredCapabilities);
//            options.setHeadless(HEADLESS);
            options.addArguments("--no-default-browser-check");

            return new FirefoxDriver(options);
        }
    }


    }

