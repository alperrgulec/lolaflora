package com.ciceksepeti.demo.test.web;

import com.ciceksepeti.demo.base.AbstractTest;
import com.ciceksepeti.demo.page.Lolaflora;
import org.junit.Test;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class WebPageTest extends AbstractTest {

    Logger logger = LoggerFactory.getLogger(this.getClass());
    private By clickSignInButton = By.xpath("//button[@class='btn btn-primary btn-lg pull-right login__btn js-login-button']");
    private By requiredFieldMailMessage = By.xpath("//span[@id='EmailLogin-error']");
    private By requiredFieldPasswordMessage = By.xpath("//span[@id='Password-error']");
    private By emailFieldId = By.xpath("//input[@id='EmailLogin']");
    private By passwordFieldId = By.xpath("//input[@id='Password']");
    private By popUp = By.xpath("//div[@class='modal-body']");
    private By popUpCloseButton = By.xpath("//button[@class='btn btn-primary']");
    private By facebookButton = By.className("membership__facebook-button");
    private By facebookPageVerifyText = By.id("loginbutton");


    Lolaflora lolaflora= new Lolaflora();

    @Test
    public void verifyLoginLolafloraPage(){
        lolaflora.launchLolaflora();
        // add verify login page control
    }
    @Test
    public void checkFieldErrorMessageClickOnButton(){
        lolaflora.launchLolaflora();
        lolaflora.click(clickSignInButton);
        lolaflora.isDisplayMessage(requiredFieldMailMessage);
        lolaflora.isDisplayMessage(requiredFieldPasswordMessage);
    }
    @Test
    public void successLogIn(){
        lolaflora.launchLolaflora();
        lolaflora.sendEmail(emailFieldId);
        lolaflora.sendPassword(passwordFieldId);
        lolaflora.click(clickSignInButton);
    }

    @Test
    public void failLogIn() throws InterruptedException {
        lolaflora.launchLolaflora();
        lolaflora.sendEmail(emailFieldId);
        lolaflora.sendIncorrectPassword(passwordFieldId);
        lolaflora.click(clickSignInButton);
        lolaflora.showPopUp(popUp);
        lolaflora.closePopUp(popUpCloseButton);
    }
    @Test
    public void redirectToFacebook(){
        lolaflora.launchLolaflora();
        lolaflora.click(facebookButton);
        lolaflora.isDisplayMessage(facebookPageVerifyText);
    }

}
